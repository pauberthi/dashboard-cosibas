# COSIBAS Dashboard

Dashboard muy básico para mostrar datos de Cosibas

## Despliegue

Para desplegar el servidor necesitamos NodeJs o en su defecto Docker.

Si tenemos NodeJs instalado, haciendo `npm install` en la carpeta root del proyecto, y luego `npm run start`.

Si preferimos utilizar Docker, ejecutamos `docker build -t cosibas-dashboard .` del el root del proyecto y luego `docker run -p 3000:3000 cosibas-dashboard`.

A partir de entonces, tendremos el dashboard accesible desde `http://localhost:3000`.


## Configuración Dashboard

El Dashboard de Cosibas está compuesto de secciones, cada una con su título y sus paneles. Toda la estructura a mostrar debe estar definida en el fichero `data.json`, donde hay un array de secciones con sus propiedades.

*section*
- `title`: [String] Título de la sección (opcional)
- `panels`: [Array] Paneles

*panel*
Propiedades comunes para todos los tipos de panel:
- `type`: [String] Tipo del panel. Valores posibles: "external" para embedir recursos externos (p.e. Grafana), "number" para mostrar números, "text" para texto plano.
- `title`: [String] Título del panel (opcional)
- `colspan`: [Number] Número de columnas que ocupará este panel, por defecto 1 (opcional)
- `rowspan`: [Number] Número de filas que ocupará este panel, por defecto 1 (opcional)

_type: "external"_
- `src`: [String] URL del recurso a mostrar

_type: "number"_
- `value`: [Number] Valor numérico a mostrar
- `text_top`: [String] Texto a mostrar justo encima del valor (opcional)
- `text_bottom`: [String] Texto a mostrar justo debajo del valor (opcional)

_type: "text"_
- `text`: [String] Texto a mostrar

