var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');


/* GET home page. */
router.get('/', (req, res) => {

  var locals = {
    title: "Dev dashboard",
    username: "Cosibas",
    sections: JSON.parse(fs.readFileSync(path.join(__dirname, '../data.json')))
  };

  return res.render('dashboard', locals);
});


module.exports = router;
